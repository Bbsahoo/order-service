package com.training.order.service;

import com.training.order.connector.OrderDataConnector;
import com.training.order.helper.OrderServiceHelper;
import com.training.order.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class OrderService {
    @Autowired
    private OrderDataConnector orderDataConnector;

    @Autowired
    private OrderServiceHelper cartServiceHelper;

    public Mono<Order> addOrder(Order order) {
        return Mono.just(order)
                .map(cartServiceHelper::setTime)
                .flatMap(orderDataConnector::createOrder)
                .log();
    }

    public Mono<Order> findOrder(String employeeId) {
        return orderDataConnector.findEmployee(employeeId);
    }
}
