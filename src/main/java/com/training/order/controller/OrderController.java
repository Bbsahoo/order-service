package com.training.order.controller;

import com.training.order.common.CommonConstants;
import com.training.order.model.Order;
import com.training.order.service.OrderService;
import io.micrometer.core.annotation.Timed;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

/**
 * @author Biswa
 * This is the cart service controller
 */
@RestController
public class OrderController {

    @Autowired
    private OrderService cartService;

    @ApiOperation(value = "Get order", produces = CommonConstants.STREAM_JSON_TYPE,
            notes = "This API used to get the order", response = Order.class)
    @ApiResponses(value = {@ApiResponse(code = 400, message = CommonConstants.BAD_REQUEST),
            @ApiResponse(code = 500, message = CommonConstants.SERVER_ERROR)})

    @GetMapping(value = "/order/{orderId}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_STREAM_JSON_VALUE})
    @ResponseStatus(code = HttpStatus.OK)
    @Timed
    public Mono<Order> findOrder(@PathVariable String orderId) {
        return cartService.findOrder(orderId);
    }

    @ApiOperation(value = "Create the Order", produces = CommonConstants.STREAM_JSON_TYPE,
            notes = "This API used to add Order", response = Order.class)
    @ApiResponses(value = {@ApiResponse(code = 400, message = CommonConstants.BAD_REQUEST),
            @ApiResponse(code = 500, message = CommonConstants.SERVER_ERROR)})

    @PostMapping(value = "/order",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_STREAM_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_STREAM_JSON_VALUE})
    @ResponseStatus(code = HttpStatus.OK)
    @Timed
    public Mono<Order> addOrder(@RequestBody(required = true) Order order) {
        return cartService.addOrder(order);
    }


}
