package com.training.order.connector;

import com.training.order.model.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Collections;
import java.util.Optional;
import java.util.function.Function;

@Component
@Slf4j
public class OrderDataConnector {

    public static final String CART_ID_PATH_PARAM = "{cartId}";
    public static final String ORDER_ID = "cartId";
    private WebClient cartDataWebClient;

    public OrderDataConnector(@Value("${order.data.service.baseUrl}") String cartDataServiceUrl) {
        cartDataWebClient = WebClient.builder().baseUrl(cartDataServiceUrl).build();
    }

    /**
     * This method is used to create the cart
     * @param order
     * @return the cart
     */
    public Mono<Order> createOrder(Order order) {
        return cartDataWebClient.post().uri(uriBuilder -> uriBuilder.pathSegment("new").build())
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(order))
                .retrieve().bodyToMono(Order.class)
                .log();
    }

    /**
     * This method is used to update the cart
     * @param order cart to be updated
     * @return the cart
     */
    public Mono<Order> updateOrder(Order order) {
        return requestBodySpec(cartDataWebClient, uriBuilder -> uriBuilder.path(CART_ID_PATH_PARAM)
                .build(Collections.singletonMap(ORDER_ID, order.getCartId())), HttpMethod.PUT)
                .body(BodyInserters.fromValue(order))
                .retrieve().bodyToMono(Order.class)
                .log();
    }

    /**
     * This method is used to delete the cart based on cart id.
     * @param orderId the cart id
     * @return
     */
    public Mono<Void> deleteOrder(String orderId) {
        return requestBodySpec(cartDataWebClient, uriBuilder -> uriBuilder.path(CART_ID_PATH_PARAM)
                .build(Collections.singletonMap(ORDER_ID, orderId)), HttpMethod.DELETE)
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .retrieve().bodyToMono(Void.class)
                .log();
    }

    /**
     * This method is used to find the cart based on cart id.
     * @param orderId the employee id
     * @return cart
     */
    public Mono<Order> findEmployee(String orderId) {
        return cartDataWebClient.get().uri(uriBuilder -> uriBuilder.path(CART_ID_PATH_PARAM).build(Collections.singletonMap(ORDER_ID, orderId)))
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .retrieve()
                .bodyToMono(Order.class)
                .log();
    }

    public static WebClient.RequestBodySpec requestBodySpec(WebClient webClient, Function<UriBuilder, URI> uriFunction, HttpMethod httpMethod) {
        return Optional.ofNullable(uriFunction)
                .map(uri -> webClient.method(httpMethod).uri(uri))
                .orElse(webClient.method(httpMethod));
    }
}
