package com.training.order.helper;

import com.training.order.model.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Slf4j
public class OrderServiceHelper {

    public Order setTime(Order cart){
        cart.setCreatedDate(new Date());
        cart.setLastUpdatedTime(new Date());
        return cart;
    }
}
